"""
rename playlist file names, removing the id, only keeping the name
"""

import os
PATH = '/run/media/klevstul/dataDrive/cloud-drive/Dropbox/gitlab/myPlaylists/playlists/spotify'
FILES = os.listdir(PATH)

for file in FILES:
    # file name format:
    # stamp-soulSailorsevenrejectstwo.1c9610e2-cc24-a96d-d45d-dda85940a5d2.csv
    file_parts = file.split('.')
    if len(file_parts) != 3:
        print(file)
        continue
    new_name = file_parts[0].replace('stamp-', '')+'.'+file_parts[2]
    print(file + '"   --->   "' + new_name)
    os.rename(os.path.join(PATH, file), os.path.join(PATH, new_name))
