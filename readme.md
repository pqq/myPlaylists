# myPlaylists

this is a repository containing playlists created by [frode burdal klevstul](http://thisworld.is)



# view my lists

- [playlists in csv format](playlists/spotify)



# play my lists

- [klevstul@spotify](https://open.spotify.com/user/klevstul)



# tools

- cover art is made using [gimp](https://www.gimp.org/) with icons from [font awesome](https://fontawesome.com/) (view [license](https://fontawesome.com/license)).
- ~~[FreeYourMusic](https://freeyourmusic.com/) is used for exporting playlists to [csv](https://en.wikipedia.org/wiki/Comma-separated_values) format.~~ UPDATE: No longer working!
- [exportify](https://rawgit.com/delight-im/exportify/master/exportify.html) is used for exporting playlists to csv.
- alternative backup solutions:
  - [exportify](https://rawgit.com/watsonbox/exportify/master/exportify.html) ([github](https://github.com/watsonbox/exportify))
  - [SpotMyBackup](http://www.spotmybackup.com/) ([github](https://github.com/secuvera/SpotMyBackup/))
  - [spotify-backup](https://github.com/caseychu/spotify-backup)
  - [tunemymusic](https://www.tunemymusic.com/) - all playlists exported as one huge file (not useful)
  - [soundiiz](https://soundiiz.com/) - paid solution



# update

- forked version of exportify with applied "download all" fix:
  - https://rawgit.com/delight-im/exportify/master/exportify.html
  - https://github.com/delight-im/exportify

